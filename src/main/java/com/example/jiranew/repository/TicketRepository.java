package com.example.jiranew.repository;

import com.example.jiranew.model.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface TicketRepository extends JpaRepository<TicketEntity,Integer> {

    TicketEntity findByName(String name);
}
