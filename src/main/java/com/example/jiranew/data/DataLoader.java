package com.example.jiranew.data;

import com.example.jiranew.model.TicketEntity;
import com.example.jiranew.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    @Qualifier(value = "ticketRepository")
    TicketRepository ticketRepository;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        TicketEntity ticketEntity1 = new TicketEntity();
        TicketEntity ticketEntity2 = new TicketEntity();
        TicketEntity ticketEntity3 = new TicketEntity();
        TicketEntity ticketEntity4 = new TicketEntity();
        TicketEntity ticketEntity5 = new TicketEntity();

        ticketEntity1.setName("Marek");
        ticketEntity1.setEmail("23123@dasd.cz");
        ticketEntity1.setCreationDatetime(LocalDateTime.now().toString());
        ticketEntity1.setIdPersonAssigned(1);
        ticketEntity1.setIdPersonCreator(1);
        ticketEntity1.setTicketCloseDatetime(LocalDateTime.now().plusMonths(1).toString());
        ticketRepository.save(ticketEntity1);

        ticketEntity2.setName("dvojka");
        ticketEntity2.setEmail("dvojka@dvojka.cz");
        ticketEntity2.setCreationDatetime(LocalDateTime.now().toString());
        ticketEntity2.setIdPersonAssigned(2);
        ticketEntity2.setIdPersonCreator(2);
        ticketEntity2.setTicketCloseDatetime(LocalDateTime.now().plusMonths(2).toString());
        ticketRepository.save(ticketEntity2);

        ticketEntity3.setName("trojka");
        ticketEntity3.setEmail("trojka@trojka.cz");
        ticketEntity3.setCreationDatetime(LocalDateTime.now().toString());
        ticketEntity3.setIdPersonAssigned(3);
        ticketEntity3.setIdPersonCreator(3);
        ticketEntity3.setTicketCloseDatetime(LocalDateTime.now().plusMonths(3).toString());
        ticketRepository.save(ticketEntity3);

        ticketEntity4.setName("ctyrka");
        ticketEntity4.setEmail("ctyri@ctyri.cz");
        ticketEntity4.setCreationDatetime(LocalDateTime.now().toString());
        ticketEntity4.setIdPersonAssigned(4);
        ticketEntity4.setIdPersonCreator(4);
        ticketEntity4.setTicketCloseDatetime(LocalDateTime.now().plusMonths(4).toString());
        ticketRepository.save(ticketEntity4);

        ticketEntity5.setName("petka");
        ticketEntity5.setEmail("petka@petka.cz");
        ticketEntity5.setCreationDatetime(LocalDateTime.now().toString());
        ticketEntity5.setIdPersonAssigned(5);
        ticketEntity5.setIdPersonCreator(5);
        ticketEntity5.setTicketCloseDatetime(LocalDateTime.now().plusMonths(5).toString());
        ticketRepository.save(ticketEntity5);

    }
}
