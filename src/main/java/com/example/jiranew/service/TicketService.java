package com.example.jiranew.service;

import com.example.jiranew.model.TicketEntity;
import com.example.jiranew.repository.TicketRepository;
import info.jira_new.spring.soapws.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class TicketService {

    @Autowired
    @Qualifier(value = "ticketRepository")
    TicketRepository ticketRepository;

    @Autowired
    TicketMapper mapper;

    public Ticket findById(Integer id){
        TicketEntity ticketEntity = ticketRepository.findById(id).orElse(null);
        return mapper.mapTo(ticketEntity);
    }

    public Ticket findByName(String name){
        TicketEntity ticketEntity = ticketRepository.findByName(name);
        return mapper.mapTo(ticketEntity);
    }

    public Ticket addTicket(Ticket ticket){
        TicketEntity entity = mapper.mapFrom(ticket);
        ticketRepository.save(entity);
        return ticket;
    }
}
