package com.example.jiranew.service;

import com.example.jiranew.model.TicketEntity;
import info.jira_new.spring.soapws.Ticket;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class TicketMapper {

    public Ticket mapTo(TicketEntity ticketEntity){
        Ticket ticket = new Ticket();
        BeanUtils.copyProperties(ticketEntity,ticket);
        return ticket;
    }

    public TicketEntity mapFrom(Ticket ticket){
        TicketEntity ticketEntity = new TicketEntity();
        BeanUtils.copyProperties(ticket,ticketEntity);
        return ticketEntity;
    }
}

