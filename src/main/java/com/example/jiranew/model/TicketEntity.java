package com.example.jiranew.model;

import javax.persistence.*;

@Entity
@Table(name="jira_tickets")
public class TicketEntity {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String email;
    @Column(name = "id_person_creator")
    private int idPersonCreator;
    @Column(name = "id_person_assigned")
    private int idPersonAssigned;
    @Column(name = "creation_datetime")
    private String creationDatetime;
    @Column(name = "ticket_close_datetime")
    private String ticketCloseDatetime;

    public TicketEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(int idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public int getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(int idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(String creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public String getTicketCloseDatetime() {
        return ticketCloseDatetime;
    }

    public void setTicketCloseDatetime(String ticketCloseDatetime) {
        this.ticketCloseDatetime = ticketCloseDatetime;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", name=" + name + ", email=" + email + ", idPersonCreator="
                + idPersonCreator + ", idPersonAssigned=" + idPersonAssigned + ", creationDatetime=" + creationDatetime + ", ticketCloseDatetime=" + ticketCloseDatetime + "]";
    }
}
