package com.example.jiranew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiraNewApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraNewApplication.class, args);
    }

}
