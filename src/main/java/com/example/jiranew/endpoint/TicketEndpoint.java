package com.example.jiranew.endpoint;

import com.example.jiranew.service.TicketService;
import info.jira_new.spring.soapws.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class TicketEndpoint {

    private static final String URI = "http://jira-new.info/spring/soapws";

    @Autowired
    @Qualifier(value = "ticketService")
    TicketService ticketService;

    @PayloadRoot(namespace = URI, localPart = "getTicketIdRequest")
    @ResponsePayload
    public GetTicketIdResponse getTicketIdResponse(@RequestPayload GetTicketIdRequest request){
        GetTicketIdResponse response = new GetTicketIdResponse();
        response.setTicket(ticketService.findById(request.getId()));
        return response;
    }

    @PayloadRoot(namespace = URI, localPart = "getTicketRequest")
    @ResponsePayload
    public GetTicketResponse getTicket(@RequestPayload GetTicketRequest request){
        GetTicketResponse response = new GetTicketResponse();
        response.setTicket(ticketService.findByName(request.getName()));
        return response;
    }

    @PayloadRoot(namespace = URI, localPart = "addTicketRequest")
    @ResponsePayload
    public AddTicketResponse addTicketResponse(@RequestPayload AddTicketRequest request){
        AddTicketResponse response = new AddTicketResponse();
        response.setTicket(ticketService.addTicket(request.getTicket()));
        return response;
    }
}
